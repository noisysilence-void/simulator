from plc import plc1,plc2,plc3,plc4,plc5,plc6
file_abs = "E:\python_file\plc1_6.txt"
import sys,os
sys.path.insert(0,os.getcwd())
from SCADA import H
from IO import *
from plant_ode.plant_ode import plant
import numpy as np
import gym


class swat():
    def __init__(self):
        self.Plant = plant()
        self.IO_P1 = P1()
        self.IO_P2 = P2()
        self.IO_P3 = P3()
        self.IO_P4 = P4()
        self.IO_P5 = P5()
        self.IO_P6 = P6()
        self.HMI = H()
        self.observation_space = gym.spaces.Box(low=-500.0, high=800.0, shape=(1, 7), dtype=np.float32)
        self.action_space = gym.spaces.Box(low=0.0, high=1.0, shape=(1, 20), dtype=np.float32)
        # self.action_space = ['0','1']
        # self.n_actions = len(self.action_space)

    def reset(self):
        return self.Plant.result

    def Action(self,P1,P2,P3,P4,P5,P6,action):         # 根据action更新IO类中的值
        if np.all(action==[0,0]):
            _action = [0 for x in range(20)]
        elif np.all(action == [0,1]):
            _action = [1 for x in range(20)]
        elif np.all(action == [1,0]):
            _action = [0 for x in range(20)]
        elif np.all(action == [1,1]):
            _action = [0 for x in range(20)]

        print(_action)

        P1.MV101.DO_Open = _action[0]
        if(_action[0]==0):
            P1.MV101.DO_Close = 1
        else:
            P1.MV101.DO_Close = 0
        P2.MV201.DO_Open = _action[1]
        if (_action[1] == 0):
            P2.MV201.DO_Close = 1
        else:
            P2.MV201.DO_Close = 0
        P3.MV301.DO_Open = _action[2]
        if (_action[2] == 0):
            P3.MV301.DO_Close = 1
        else:
            P3.MV301.DO_Close = 0
        P3.MV302.DO_Open = _action[3]
        if (_action[3] == 0):
            P3.MV302.DO_Close = 1
        else:
            P3.MV302.DO_Close = 0
        P3.MV303.DO_Open = _action[4]
        if (_action[4] == 0):
            P3.MV303.DO_Close = 1
        else:
            P3.MV303.DO_Close = 0
        P3.MV304.DO_Open = _action[5]
        if (_action[5] == 0):
            P3.MV304.DO_Close = 1
        else:
            P3.MV304.DO_Close = 0
        P5.MV501.DO_Open = _action[6]
        if (_action[6] == 0):
            P5.MV501.DO_Close = 1
        else:
            P5.MV501.DO_Close = 0
        P5.MV502.DO_Open = _action[7]
        if (_action[7] == 0):
            P5.MV502.DO_Close = 1
        else:
            P5.MV502.DO_Close = 0
        P5.MV503.DO_Open = _action[8]
        if (_action[8] == 0):
            P5.MV503.DO_Close = 1
        else:
            P5.MV503.DO_Close = 0
        P5.MV504.DO_Open = _action[9]
        if (_action[9] == 0):
            P5.MV504.DO_Close = 1
        else:
            P5.MV504.DO_Close = 0

        P1.P101.DO_Start = _action[10]
        P1.P102.DO_Start = _action[11]
        P3.P301.DO_Start = _action[12]
        P3.P302.DO_Start = _action[13]
        P4.P401.DO_Start = _action[14]
        P4.P402.DO_Start = _action[15]
        P5.P501_VSD_Out.Start = _action[16]
        if (_action[16] == 0):
            P5.P501_VSD_Out.Stop = 1
        else:
            P5.P501_VSD_Out.Stop = 0
        P5.P502_VSD_Out.Start = _action[17]
        if (_action[17] == 0):
            P5.P502_VSD_Out.Stop = 1
        else:
            P5.P502_VSD_Out.Stop = 0
        P6.P601.DO_Start = _action[18]
        P6.P602.DO_Start = _action[19]

    def step(self,action,k):
        self.Action(self.IO_P1, self.IO_P2, self.IO_P3, self.IO_P4, self.IO_P5, self.IO_P6,action)

        self.Plant.Actuator(self.IO_P1, self.IO_P2, self.IO_P3, self.IO_P4, self.IO_P5, self.IO_P6)        # 根据更新后的IO类更新actuator
        # print(self.IO_P1,self.IO_P2,self.IO_P3,self.IO_P4,self.IO_P5,self.IO_P6)
        self.Plant.Plant(self.IO_P1, self.IO_P2, self.IO_P3, self.IO_P4, self.IO_P5, self.IO_P6,k)
        print(self.Plant.result)# 根据actuator更新sensor
        p = {"LIT101_AL": 500, "LIT101_AH": 800, "LIT301_AL": 500, "LIT301_AH": 800, "LIT401_AL": 500, "LIT401_AH": 800,
             "LIT601_AL": 500, "LIT601_AH": 800, "LIT602_AL": 500, "LIT602_AH": 800, "LIT101_ALL": 250,
             "LIT101_AHH": 1000,
             "LIT301_ALL": 250, "LIT301_AHH": 1000, "LIT401_ALL": 250, "LIT401_AHH": 1000}
        self.HMI.LIT101.AH = self.Plant.result[2] > p["LIT101_AH"]
        self.HMI.LIT101.AL = self.Plant.result[2] < p["LIT101_AL"]
        self.HMI.LIT301.AH = self.Plant.result[3] > p["LIT301_AH"]
        self.HMI.LIT301.AL = self.Plant.result[3] < p["LIT301_AL"]
        self.HMI.LIT401.AH = self.Plant.result[4] > p["LIT401_AH"]
        self.HMI.LIT401.AL = self.Plant.result[4] < p["LIT401_AL"]
        self.HMI.LIT101.AHH = self.Plant.result[2] > p["LIT101_AHH"]

        self.HMI.LIT101.ALL = self.Plant.result[2] < p["LIT101_ALL"]
        self.HMI.LIT301.AHH = self.Plant.result[3] > p["LIT301_AHH"]
        self.HMI.LIT401.AHH = self.Plant.result[4] > p["LIT401_AHH"]
        self.HMI.LIT401.ALL = self.Plant.result[4] < p["LIT401_ALL"]

        if(self.HMI.LIT101.AH == 0 and self.HMI.LIT101.AL == 0 and self.HMI.LIT301.AH == 0 and
                self.HMI.LIT301.AL == 0 and self.HMI.LIT401.AH == 0 and self.HMI.LIT401.AL == 0 and
                self.HMI.LIT101.AHH ==0 and self.HMI.LIT101.ALL == 0 and self.HMI.LIT301.AHH == 0 and
                self.HMI.LIT401.AHH == 0 and self.HMI.LIT401.ALL == 0):
            reward = 0
            done = False
        else:
            reward = 1
            done = True

        self.Plant.result = np.array(self.Plant.result)

        return self.Plant.result,reward,done

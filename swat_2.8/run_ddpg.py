
import numpy as np
import tensorflow as tf
from swat_env import swat
from src.wolp_agent import WolpertingerAgent


from src.wolp_agent import *             # class: WolpertingerAgent-get_name,get_action_space,act,wolp_action,
from src.ddpg.agent import DDPGAgent       #
import src.util.data
from src.util.timer import Timer


env = swat()
MEMORY_SIZE = 3000
ACTION_SPACE = 4

sess = tf.Session()



with tf.variable_scope('WolpertingerAgent'):
    wolp_agent = WolpertingerAgent(env, max_actions=1000, k_ratio=0.1)


sess.run(tf.global_variables_initializer())


def train(RL):
    total_steps = 0
    observation = np.array([0,0,650,650,650,200,200], dtype = int)       # [0,0,650,650,650,200,200]

    steps = env.spec.timestep_limit
    timer = Timer()

    data = src.util.data.Data()
    data.set_agent(agent.get_name(), int(agent.action_space.get_number_of_actions()),
                   agent.k_nearest_neighbors, 3)
    # data.set_experiment(experiment, agent.low.tolist(), agent.high.tolist(), episodes)

    agent.add_data_fetch(data)
    print(data.get_file_name())

    full_epoch_timer = Timer()
    reward_sum = 0

    episodes = 2500
    for ep in range(episodes):

        timer.reset()
        observation = env.reset()

        total_reward = 0
        print('Episode ', ep, '/', episodes - 1, 'started...', end='')
        for t in range(steps):


            action = agent.act(observation)

            data.set_action(action.tolist())

            data.set_state(observation.tolist())

            prev_observation = observation
            observation, reward, done, info = env.step(action[0] if len(action) == 1 else action)

            data.set_reward(reward)

            episode = {'obs': prev_observation,
                       'action': action,
                       'reward': reward,
                       'obs2': observation,
                       'done': done,
                       't': t}

            agent.observe(episode)

            total_reward += reward

            if done or (t == steps - 1):
                t += 1
                reward_sum += total_reward
                time_passed = timer.get_time()
                print('Reward:{} Steps:{} t:{} ({}/step) Cur avg={}'.format(total_reward, t,
                                                                            time_passed, round(
                        time_passed / t),
                                                                            round(reward_sum / (ep + 1))))

                data.finish_and_store_episode()

                break
    # end of episodes
    time = full_epoch_timer.get_time()
    print('Run {} episodes in {} seconds and got {} average reward'.format(
        episodes, time / 1000, reward_sum / episodes))

    data.save()


if __name__ == "__main__":
    env = swat()
    # RL = DoubleDQN(n_actions=ACTION_SPACE, n_features=7, memory_size=MEMORY_SIZE,
    #    e_greedy_increment=0.001, double_q=True, sess=sess, output_graph=False)

    RL = train(wolp_agent)
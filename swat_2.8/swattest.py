#  This is the Main function
import sys,os
sys.path.insert(0,os.getcwd())
from io_plc.IO_PLC import DI_WIFI
from SCADA import H
from IO import *
from plc import plc1,plc2,plc3,plc4,plc5,plc6

from plant_ode.plant_ode import plant 
import sys
import zmq
import collections



ip = "10.0.1.91"
port = "5563"
port2 = "5564"

tcp_address = "tcp://" + ip + ":" + port
tcp_address2 = "tcp://" + ip + ":" + port2
address = "ipc://sensors.ipc"
f = open('test.log','w')
# Socket to talk to server
context = zmq.Context()

socket = context.socket(zmq.SUB)

socket.connect (tcp_address)
socket.connect (tcp_address2)

socket.setsockopt(zmq.SUBSCRIBE,'')


string = socket.recv()
fields = string.split(',')
listing = collections.deque(fields)

while True:
        string = socket.recv()
        fields = string.split(',')
        
        port = fields[0]
        
        if port == "5563":
                L1_p1_auto = fields[1]
                L1_p1_start = fields[2]
                L1_p1_stop = fields[3]
                L1_p2_auto = fields[4]
                L1_p2_start = fields[5]
                L1_p2_stop = fields[6]
                L1_mv1_open = fields[7]
                L1_mv1_close = fields[8]
                L1_level = fields[9]
                L1_flow = fields[10]
                L1_level_raw = fields[11]
                L1_flow_raw = fields[12]
                f.write( "\nFrom L1 Network = "+L1_p1_auto+","+L1_p1_start+","+L1_p1_stop+","+L1_p2_auto+","+L1_p2_start+","+L1_p2_stop+","+L1_mv1_open+","+L1_mv1_close+","+L1_level+","+L1_flow)
                
                print "From L1 Network = "+L1_p1_auto+","+L1_p1_start+","+L1_p1_stop+","+L1_p2_auto+","+L1_p2_start+","+L1_p2_stop+","+L1_mv1_open+","+L1_mv1_close+","+L1_level+","+L1_flow+","+L1_level_raw +","+L1_flow_raw 
        elif port == "5564":
                IC_p1_auto = fields[1]
                IC_p1_start = fields[2]
                IC_p1_stop = fields[3]
                IC_p2_auto = fields[4]
                IC_p2_start = fields[5]
                IC_p2_stop = fields[6]
                IC_mv1_open = fields[7]
                IC_mv1_close = fields[8]
                IC_level = fields[9]
                IC_flow = fields[10]
                IC_level_raw = fields[11]
                IC_flow_raw = fields[12]
                IC_time = fields[13]
                print "From PiiC       = "+IC_p1_auto+","+IC_p1_start+","+IC_p1_stop+","+IC_p2_auto+","+IC_p2_start+","+IC_p2_stop+","+IC_mv1_open+","+IC_mv1_close+","+IC_level+","+IC_flow+","+IC_level_raw +","+IC_flow_raw  
                f.write( "\nFrom PiiC       = "+IC_p1_auto+","+IC_p1_start+","+IC_p1_stop+","+IC_p2_auto+","+IC_p2_start+","+IC_p2_stop+","+IC_mv1_open+","+IC_mv1_close+","+IC_level+","+IC_flow+","+IC_time )

        maxstep = 200*60*30#*60#*2 # time is counted in 0.005 seconds, 200*x*y, x unit seconds, y unit minutes
        # Initiating Plant
        Plant = plant(maxstep)
        # Defining I/O
        IO_DI_WIFI = DI_WIFI()
        IO_P1 = P1()
        # IO_P1.FIT101.AI_Value=AI_FIT_101_FLOW
        # IO_P1.LIT101.AI_Value=P1.AI_LIT_101_LEVEL
        IO_P1.FIT101.AI_Value=float(L1_flow_raw)
        IO_P1.LIT101.AI_Value=float(L1_level_raw)
        IO_P1.MV101.DI_ZSC=int(L1_mv1_close)
        IO_P1.MV101.DI_ZSO=int(L1_mv1_open)
        IO_P1.P101.DI_Auto=int(L1_p1_auto)
        IO_P1.P101.DI_Fault=int(L1_p1_stop)
        IO_P1.P101.DI_Run=int(L1_p1_start)
        IO_P1.P102.DI_Auto=int(L1_p2_auto)
        IO_P1.P102.DI_Fault=int(L1_p2_stop)
        IO_P1.P102.DI_Run=int(L1_p2_start)
        # IO_P2 = P2()
        # IO_P3 = P3()
        # IO_P4 = P4()
        # IO_P5 = P5()
        # IO_P6 = P6()
        HMI = H()
        PLC1 = plc1.plc1(HMI)
        # PLC2 = plc2.plc2(HMI)
        # PLC3 = plc3.plc3(HMI)
        # PLC4 = plc4.plc4(HMI)
        # PLC5 = plc5.plc5(HMI)
        # PLC6 = plc6.plc6(HMI)
        # Main Loop Body
        # for time in range(maxstep):	
        #Second, Minute and Hour pulse
        # Sec_P = not bool(time%(200))
        # Min_P = not bool(time%(200*60))
        # Hrs_P = not bool(time%(200*60*60))
        Sec_P = 0
        Min_P = 0
        Hrs_P = 0
        #solving out plant odes in 5 ms
        # 	Plant.Actuator(IO_P1,IO_P2,IO_P3,IO_P4,IO_P5,IO_P6)
        # 	Plant.Plant(IO_P1,IO_P2,IO_P3,IO_P4,IO_P5,IO_P6,time)
        #PLC working
        PLC1.Pre_Main_Raw_Water(IO_DI_WIFI,IO_P1,HMI,Sec_P,Min_P,Hrs_P)
        print HMI.LIT101.Pv
        print HMI.FIT101.Pv
        # 	PLC2.Pre_Main_UF_Feed_Dosing(IO_DI_WIFI,IO_P2,HMI,Sec_P,Min_P,Hrs_P)	
        # 	PLC3.Pre_Main_UF_Feed(IO_DI_WIFI,IO_P3,HMI,Sec_P,Min_P,Hrs_P)
        # 	PLC4.Pre_Main_RO_Feed_Dosing(IO_DI_WIFI,IO_P4,HMI,Sec_P,Min_P,Hrs_P)
        # 	PLC5.Pre_Main_High_Pressure_RO(IO_DI_WIFI,IO_P5,HMI,Sec_P,Min_P,Hrs_P)
        # 	PLC6.Pre_Main_Product(IO_DI_WIFI,IO_P6,HMI,Sec_P,Min_P,Hrs_P)
        # 	print ('{0}\n'.format(Plant.result[time][2:]))
        #print IO_P3.P301.DI_Run or IO_P3.P302.DI_Run and IO_P3.MV301.DI_ZSC and IO_P3.MV302.DI_ZSO and IO_P3.MV303.DI_ZSC and IO_P3.MV304.DI_ZSC and IO_P6.P602.DI_Run 
        #print HMI.Cy_P3.UF_FILTRATION_MIN
f.close()


"""
Reinforcement learning maze example.

Red rectangle:          explorer.
Black rectangles:       hells       [reward = -1].
Yellow bin circle:      paradise    [reward = +1].
All other states:       ground      [reward = 0].

This script is the main part which controls the update method of this example.
The RL is in RL_brain.py.

View more on my tutorial page: https://morvanzhou.github.io/tutorials/
"""

from RL_brain import DoubleDQN
import numpy as np
import tensorflow as tf
from swat_env import swat
from src.wolp_agent import WolpertingerAgent


env = swat()
MEMORY_SIZE = 3000
ACTION_SPACE = 4

sess = tf.Session()

with tf.variable_scope('Double_DQN'):
    double_DQN = DoubleDQN(
        n_actions=ACTION_SPACE, n_features=7, memory_size=MEMORY_SIZE,
        e_greedy_increment=0.001, double_q=True, sess=sess, output_graph=True)

with tf.variable_scope('WolpertingerAgent'):
    agent = WolpertingerAgent(env, max_actions=1000, k_ratio=0.1)


sess.run(tf.global_variables_initializer())


def train(RL):
    total_steps = 0
    observation = np.array([0,0,650,650,650,200,200], dtype = int)       # [0,0,650,650,650,200,200]
    while True:
        # if total_steps - MEMORY_SIZE > 8000: env.render()

        action = RL.choose_action(observation)
        print(action)

        # f_action = (action-(ACTION_SPACE-1)/2)/((ACTION_SPACE-1)/4)   # convert to [-2 ~ 2] float actions
        k = 0
        observation_, reward, done = env.step(np.array([action]),k)
        k += 1
        print(observation_,reward,done)

        # reward /= 10     # normalize to a range of (-1, 0). r = 0 when get upright
        # the Q target at upright state will be 0, because Q_target = r + gamma * Qmax(s', a') = 0 + gamma * 0
        # so when Q at this state is greater than 0, the agent overestimates the Q. Please refer to the final result.
        print(observation,action,reward,observation_)
        RL.store_transition(observation, action, reward, observation_)
        print("store_transition")

        if total_steps > MEMORY_SIZE:   # learning
            RL.learn()

        if total_steps - MEMORY_SIZE > 20000:   # stop game
            break

        observation = observation_
        total_steps += 1
    return RL.q


if __name__ == "__main__":
    env = swat()
    # RL = DoubleDQN(n_actions=ACTION_SPACE, n_features=7, memory_size=MEMORY_SIZE,
    #    e_greedy_increment=0.001, double_q=True, sess=sess, output_graph=False)

    RL = train(double_DQN)
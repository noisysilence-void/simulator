This is a software for simulating the Secure Water Treatment testbed (SWaT) system located at the Singapore University of Technology and Design, under iTrust.
The software should contain three blocks from the top level: (1) the PLC logic; (2) the HMI, or the SCADA module; and (3) the plant model.
The SWaT system has 6 PLCs, each connected to the HMI with a star structure. The plant is also divided into 6 phases, and communicate exclusively with a PLC in both ways.
The main function of this software is swat.py: you can run it by typing:
	python swat.py
in a terminal.
The simulation period is 5ms, same as the SWaT system.
From:https://sav.sutd.edu.sg/research/physical-attestation/sp-2018-paper-supplementary-material/

In the following work,I will replace the part of the PLC with the DRL code,to find the attack to the SWAT which makes sense after a time interval.
After modification,the main function of our code is run_this.py:you can run it by typing:
	python run_this.py
in a terminal.
